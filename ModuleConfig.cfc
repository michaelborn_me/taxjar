component{
	// Module Properties
	this.title      = "cfTaxjar";
	this.author       = "Michael Born <mike@learncf.dev>";
	this.webURL       = "https://forgebox.io/view/cftaxjar";
	this.description    = "CFML wrapper for the Taxjar API";
	this.entryPoint     = "cftaxjar";
	this.inheritEntryPoint = true;
	this.autoMapModels = true;
	this.autoProcessModels = false;

	function configure(){
		settings = {
			APIToken = "MY_API_KEY"
		};
	}

}