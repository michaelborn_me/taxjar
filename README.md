# TaxJar API Integration

Use [TaxJar][1] for retrieving tax rates by county, state, country, etc.

## Getting Started

### ColdBox Setup

1. Install cftaxjar - `box install cfTaxjar`
2. Add a module setting to set the api key:

```js
moduleSettings = {
    cfTaxjar: {
        APIToken: "MY_API_KEY"
    }
}
```

3. Inject cftaxjar - `property name="cftaxjar" inject="taxjar@cftaxjar";`

### Vanilla CFML Setup

1. Install cftaxjar - `box install cfTaxjar
2. Initialize the taxjar component- `var cfTaxjar = new cftaxjar.models.taxjar()`
3. Set the API key - `cfTaxjar.setAPIToken( 'MY_API_KEY' )`

## Docs

### Load Integration & Set API Key

```js
TaxJar = New taxjar.taxjar( application.apiKey );
```

### Get Tax Rates

Right now there's only a single method to retrieve tax rates: `getTaxRatesByLocation()`.

```js
TaxJar.getTaxRatesByLocation( ... );
```

#### Get By Zip

```js
TaxJar.getTaxRatesByLocation( myZipCode );
```

#### Get By Address

Please note that `zip` is still required.

```js
TaxJar.getTaxRatesByLocation(
    street = "Pennsylvania Ave",
    city = "Washington",
    country = "US",
    zip = "20500"
);
```

## TODO

* Add ColdBox support ✅
* Add [DocBox][2]-style documentation ❌
* Write [TestBox][3] tests ❌

[1]: https://www.taxjar.com/
[2]: https://github.com/Ortus-Solutions/DocBox/wiki
[3]: https://testbox.ortusbooks.com/
