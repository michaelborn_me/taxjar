/**
 * TaxJar API Integration
 * API Reference: https://developers.taxjar.com/api/reference/
 * @author Michael Born
 * @date May 2018
 */
component accessors="true" {
	/**
	 * API token is how we authenticate to taxjar.
	 * This can be set via setApiToken( string ) or injected via Wirebox.
	*/
	property
		name="apiToken"
		type="string"
		inject="box:setting:APIToken@cftaxjar";

	/**
	 * The API base URL - probably don't need to change this unless taxjar changes their API url.
	*/
	property
		name="apiURL"
		type="string"
		default="https://api.taxjar.com";

	/**
	 * The API version - sent to taxjar, doesn't change this library's behavior at all.
	*/
	property
		name="apiVersion"
		type="string"
		default="v2";

	public component function init(){
		return this;
	}

	private function _parseRequest(required httpReq) {
		if ( arguments.httpReq.responseheader.status_code > "299" ) {
			var retval = {"error":true,"message":"API returned status #arguments.httpReq.responseheader.status_code# #arguments.httpReq.responseheader.explanation#"};
		} else if ( arguments.httpReq.filecontent == "" ) {
			var retval = {"error":true,"message":"No response from API"};
		} else {
			var retval = httpReq.filecontent;
		}
		return deSerializeJSON(retval);
	}

	private function _sendRequest(endpoint="", urlData="", formData="", method="GET", body="") {
		var requestURL = "#variables._apiURL#/#variables._apiVersion#/#arguments.endpoint#";
		cfhttp( url=requestURL, result="response", method=arguments.method ) {
			if ( arguments.body != "" ) {
				cfhttpparam( type="body", value=arguments.body );
			}
			cfhttpparam( name="authorization", type="header", value="Bearer #variables._apiToken#" );
			for ( curArg in arguments.urlData ) {
				cfhttpparam( name=LCase(curArg), type="url", value=arguments['urlData'][curArg] );
			}
			for ( curArg in arguments.formData ) {
				cfhttpparam( name=LCase(curArg), type="formfield", value=arguments['formData'][curArg] );
			}
		}
		return variables._parseRequest(response);
	}

	/**
	 * Get tax rates by zip or address.
	 * Notice that state is not included in the address. Please see https://developers.taxjar.com/api/reference/##get-show-tax-rates-for-a-location for documentation.
	 */
	public function getTaxRatesByLocation(required zip, city, street, country) {
		var params = {};
		if ( StructKeyExists(arguments,"city") ) {
			params.city = arguments.city;
		}
		if ( StructKeyExists(arguments,"street") ) {
			params.street = arguments.street;
		}
		if ( StructKeyExists(arguments,"country") ) {
			params.country = arguments.country;
		}
		
		return variables._sendRequest(
			endpoint = "/rates/" & arguments.zip,
			urlData = params
		);
	}

}